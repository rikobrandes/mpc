import launch
import launch.actions
import launch.substitutions
import launch_ros.actions

from launch_ros.default_launch_description import ROSSpecificLaunchStartup

import ament_index_python


def generate_launch_description():

    #####################
    ### mpc_controller ###
    ######################
    mpc_controller_node = launch_ros.actions.Node(
        package="mpc_controller_node",
        node_executable="mpc_controller_node_exe",
        node_name="mpc_controller",
        parameters=[
            "{}/defaults.yaml".format(
                ament_index_python.get_package_share_directory(
                    "mpc_controller_node"
                )
            ),
            # overwrite parameters from yaml here
            {
                "command_topic" :  "vehicle_command",
                #"state_topic" : "debug_state",
                "controller.interpolation": True,
                "controller.sample_tolerance_ms": 20,
                "controller.control_lookahead_ms": 100,
                "controller.limits.min_longitudinal_velocity_mps": 0.01,
                "controller.limits.max_longitudinal_velocity_mps": 35.0,
                # jerk is used as limit for longitudinal control for whatever reason, see
                # https://gitlab.com/aninnymouse/mpc/-/blob/master/control/mpc_controller/src/mpc_controller/mpc_controller.cpp#L280
                "controller.limits.min_jerk_mps3": -3.0,
                "controller.limits.max_jerk_mps3": 3.0,
                # And also steer angle RATE for lateral control
                "controller.limits.min_steer_angle_rate_rps": -0.331,
                "controller.limits.max_steer_angle_rate_rps": 0.331,
                "controller.vehicle.cg_to_front_m": 1.2,
                "controller.vehicle.cg_to_rear_m": 1.5,
                "controller.behavior.stop_rate_mps2": 3.0,
                "controller.behavior.time_step_ms": 100,
                "controller.behavior.is_temporal_reference": True,
                "controller.weights.nominal.pose": 10.0,
                "controller.weights.nominal.heading": 10.0,
                "controller.weights.nominal.longitudinal_velocity": 10.0,
                "controller.weights.terminal.pose": 1000.0,
                "controller.weights.terminal.heading": 1000.0,
                "controller.weights.terminal.longitudinal_velocity": 1000.0,
            }
        ],
        output='screen',
    )

    ld = launch.LaunchDescription([
        ROSSpecificLaunchStartup(),
        mpc_controller_node

        # Wait for the lgsvl_interface to transition to active state
        #event_handler_lgsvl_interface_active,
        #launch.actions.OpaqueFunction(function=lambda context: ready_fn()),
    ])

    # An array of all the checkers to be enumerated by the tests
    return ld
